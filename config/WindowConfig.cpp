/*
File: WindowConfig.cpp

About: Defines behaviour for the WindowConfig class. This class is static and not instantiated.
	   The variables and methods contained are used to manage the window dimentions (resizing sprites, finding window hitbox, ect.)
	   The contents of this class are used to ensure all sprite ratios and positions are relative to the dimensions of the window.

	   When setting and modifying entity/hitbox/sprite locations and sizes, consider the window to be a 1000 x 1000 
	   (maybe expand to make more precise) unit grid.
	   By using the convertToPixels() method, the values stored will be converted into pixels based on the current window 
	   dimensions before being drawn.
*/

#include "../header/config/WindowConfig.h"

int WindowConfig::prevW = 1;
int WindowConfig::prevH = 1;
int WindowConfig::w = 1;
int WindowConfig::h = 1;

/*
@method
@description - Initializes the WindowConfig variables. As WindowConfig is a static object, this is done without a constructor.
			   This should be done when the window is first created and kept up to date using the update() method.
@param SDL_Window* window - Pointer to the window containing the game (instantiated in the main method).
*/
void WindowConfig::init(SDL_Window* window) {
	int winW;
	int winH;
	SDL_GetWindowSize(window, &winW, &winH);
	WindowConfig::prevH = h = winH;
	WindowConfig::prevW = w = winW;
	//hitbox = Hitbox(0, 0, winW, winH);
}

/*
@method
@description - Updates the WindowConfig variables. This should be called when the dimensions of the window are changed.
@param int w - New width of the window.
@param int h - New height of the window.
*/
void WindowConfig::update(int w, int h) {
	WindowConfig::prevW = WindowConfig::w;
	WindowConfig::prevH = WindowConfig::h;
	WindowConfig::w = w;
	WindowConfig::h = h;
	//hitbox = Hitbox(0, 0, w, h);
}
/*
@method
@description - Uses pointers to convert entity units to pixels.
@param int x_w* - Either the x or w value to be converted (uses window width for conversion).
@param int y_h* - Either the y or h value to be converted (uses window height for conversion).
*/
void WindowConfig::convertToPixels(int* x_w, int* y_h) {
	*x_w = w * *x_w / 1000;
	*y_h = h * *y_h / 1000;
}