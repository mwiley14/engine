/*
File: Sprite.cpp

About: Defines the behaviour for the Sprite class. The sprite class contains information for an entity sprite and methods for drawing the
	   sprite to the display.
*/

#include "../../header/entity/abstract/Sprite.h"

/*
@method
@description - Default constructor for the Sprite class. Initializes the z-index to 0.
@parameter SDL_Renderer* renderer - Pointer to the graphics renderer.
@parameter string src - Relative path to the sprite's image file.
@parameter int x - The x position of the sprite.
@parameter int y - The y position of the sprite.
@parameter int w - The width of the sprite.
@parameter int h - The height of the sprite.
*/
Sprite::Sprite(SDL_Renderer* renderer, string src, int x, int y, int w, int h) : Sprite(renderer, src, x, y, w, h, 0) {

}

/*
@method
@description - Constructor for the Sprite class.
@parameter SDL_Renderer* renderer - Pointer to the graphics renderer.
@parameter string src - Relative path to the sprite's image file.
@parameter int x - The x position of the sprite.
@parameter int y - The y position of the sprite.
@parameter int w - The width of the sprite.
@parameter int h - The height of the sprite.
@parameter int zIndex - The depth of the sprite (Lower number is animated farther back).
*/
Sprite::Sprite(SDL_Renderer* renderer, string src, int x, int y, int w, int h, int zIndex) {
	SDL_Surface* surface = IMG_Load(src.c_str());
	if (!surface) {
		printf("Image not succefully loaded: %s\n", SDL_GetError());
	}
	texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);
	if (!texture) {
		printf("Texture not successfully loaded: %s\n", SDL_GetError());
	}
	SDL_QueryTexture(texture, NULL, NULL, &sprite.w, &sprite.h);
	hitbox = Hitbox(x, y, w, h);
	this->x = x;
	this->y = y;
	this->w = w;
	this->h = h;
	this->zIndex = zIndex;
}

/*
@method
@description - Setter for the x position of the sprite. Updates the position of the sprite's hitbox as well.
@parameter int x - The new x position of the sprite.
*/
void Sprite::setX(int x) {
	this->x = x;
	hitbox.x = x;
}

/*
@method
@description - Setter for the y position of the sprite. Updates the position of the sprite's hitbox as well.
@parameter int y - The new y position of the sprite.
*/
void Sprite::setY(int y) {
	this->y = y;
	hitbox.y = y;
}

/*
@method
@description - Setter for the width of the sprite. Updates the width of the sprite's hitbox as well.
@parameter int w - The new width of the sprite.
*/
void Sprite::setW(int w) {
	this->w = w;
	hitbox.w = w;
}

/*
@method
@description - Setter for the height of the sprite. Updates the height of the sprite's hitbox as well.
@parameter int h - The new height of the sprite.
*/
void Sprite::setH(int h) {
	this->h = h;
	hitbox.h = h;
}

/*
@method
@description - Adds the sprite to the graphics renderer in preperation for the next frame rendering.
@parameter SDL_Renderer* renderer - Pointer to the graphics renderer.
*/
void Sprite::draw(SDL_Renderer* renderer) {
	resize();
	if (hitbox.intersect(Hitbox(0, 0, WindowConfig::w, WindowConfig::h))) {
		SDL_RenderCopy(renderer, texture, NULL, &sprite);
	}
}

/*
@method
@override
@description - Resizes the SDL_Rect (sprite) position and measurements to be relative to the window ratios.
			   Called before being drawn. Object position and measurements are left unchanged.
*/
void Sprite::resize() {
	hitbox.resize();
	sprite.x = x;
	sprite.y = y;
	sprite.w = w;
	sprite.h = h;
	WindowConfig::convertToPixels(&sprite.x, &sprite.y);
	WindowConfig::convertToPixels(&sprite.w, &sprite.h);
}