/*
File: Entity.cpp

About: Defines behaviour for the Entity abstract class. All entities inherit from this class and it includes general methods for usage
	   and outlines the required methods to override.
*/

#include "../../header/entity/abstract/Entity.h"

/*
@method
@description - Constructor for Entity class.
@parameter int x - The x position of the entity.
@parameter int y - The y position of the entity.
@parameter int w - The width of the entity.
@parameter int h - The height of the entity.
*/
Entity::Entity(int x, int y, int w, int h) : hitbox(x, y, w, h) {
	removed = false;
}

/*
@method
@description - Sets the sprite variable of the entity to the one provided.
@parameter Sprite sprite - The sprite to attach to the entity
*/
void Entity::setSprite(Sprite sprite) {
	this->sprite = sprite;
}

/*
@method
@description - Checks if the entity has been registered for removal from the entities list.
@return bool - Returns the value of the removed, true if registered for removal, false if not.
*/
bool Entity::isRemoved() {
	return removed;
}

/*
@method
@virtual
@description - Checks if the entity has a sprite to draw.
@return bool - Returns true by default.
*/
bool Entity::isDrawable() {
	return true;
}

/*
@method
@virtual
@description - Checks if the entity has events to run.
@return bool - Returns true by default.
*/
bool Entity::hasEvents() {
	return true;
}

/*
@method
@description - Registers the entity in the entities list and notifies the frame cycle to sort the entities by z-index.
*/
void Entity::registerEntity() {
	if (this->isDrawable()) {
		newEntity = true;
	}
	entities.push_back(shared_ptr<Entity>(this));
}

/*
@method
@virtual
@description - Runs standard events for the entity.
*/
void Entity::runEvents() {

}

/*
@method
@virtual
@description - Runs SDL based events.
@parameter SDL_Event event - The SDL_Event currently active.
*/
void Entity::runSDLEvents(SDL_Event event) {
	
}

/*
@method
@virtual
@description - Runs keyboard state events.
@parameter const Uint8* keyboard - The state of the keyboard (SDL_SCANCODE flags).
*/
void Entity::runKeyboardEvents(const Uint8* keyboard) {

}

/*
@method
@description - Registers the entity for removal.
*/
void Entity::remove() {
	removed = true;
	removable.push_back(shared_ptr<Entity>(this));
}