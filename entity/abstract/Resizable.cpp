/*
File: Resizable.cpp

About: Defines behaviour for the Resizable class. This class acts as an interface for any resizable object (which in this case are all
	   sprites). Resizing is applied every frame where a proper pixel ratio is needed, after the logic is done.
*/

#include "../../header/entity/abstract/Resizable.h"

/*
@method
@virtual
@description - This method is called to change any class variables of an object that inherits it that need to be relative to the window
			   size. This is done by refering to the current state of the WindowConfig object.
*/
void Resizable::resize() {

}