
#include "../header/entity/Player.h"

Player::Player(SDL_Renderer* renderer, string src, int x, int y, int w, int h, int zIndex) : Entity(x, y, w, h) {
	entityId = PLAYER_ID;
	setSprite(Sprite(renderer, src, x, y, w, h, zIndex));
	registerEntity();
}

void Player::spawn(SDL_Renderer* renderer, string src, int x, int y, int w, int h, int zIndex) {
	//entities.push_back(shared_ptr<Entity>(new Player(renderer, src, x, y, w, h, zIndex)));
	new Player(renderer, src, x, y, w, h, zIndex);
}

void Player::runKeyboardEvents(const Uint8* keyboard) {
	printf("Player Entity ID: %i\n", entityId);
	int xOffset = 0;
	int yOffset = 0;
	if (keyboard[SDL_SCANCODE_UP] || keyboard[SDL_SCANCODE_W]) {
		yOffset -= ySpeed;
		if (hitbox.y < room->hitbox.y) {
			yOffset += ySpeed; //TODO: Shift based on distance, not just back to where it was.
		}
	}
	if (keyboard[SDL_SCANCODE_LEFT] || keyboard[SDL_SCANCODE_A]) {
		xOffset -= xSpeed;
		if (hitbox.x < room->hitbox.x) {
			xOffset += xSpeed;
		}
	}
	if (keyboard[SDL_SCANCODE_DOWN] || keyboard[SDL_SCANCODE_S]) {
		yOffset += ySpeed;
		if (hitbox.y + hitbox.h > room->hitbox.y + room->hitbox.h) {
			yOffset -= ySpeed;
		}
	}
	if (keyboard[SDL_SCANCODE_RIGHT] || keyboard[SDL_SCANCODE_D]) {
		xOffset += xSpeed;
		if (hitbox.x + hitbox.w > room->hitbox.x + room->hitbox.w) {
			xOffset -= xSpeed;
		}
	}
	sprite->setX(sprite->x + xOffset);
	sprite->setY(sprite->y + yOffset);
	hitbox.x += xOffset;
	hitbox.y += yOffset;
	int xWindowOffset = 0;
	int yWindowOffset = 0;
	if (hitbox.x < room->innerBounds.x) {
		xWindowOffset = xOffset * -1;
		if (room->hitbox.x + xOffset > 0) {
			xWindowOffset = 0;
		}
	}
	if (hitbox.y < room->innerBounds.y) {
		yWindowOffset = yOffset * -1;
		if (room->hitbox.y + yOffset > 0) {
			yWindowOffset = 0;
		}
	}
	if (hitbox.x + hitbox.w > room->innerBounds.x + room->innerBounds.w) {
		xWindowOffset = xOffset * -1;
		if (room->hitbox.x + room->hitbox.w < 1000) {
			xWindowOffset = 0;
		}
	}
	if (hitbox.y + hitbox.h > room->innerBounds.y + room->innerBounds.h) {
		yWindowOffset = yOffset * -1;
		if (room->hitbox.y + room->hitbox.h + yOffset < 1000) {
			yWindowOffset = 0;
		}
	}
	if (xWindowOffset != 0 || yWindowOffset != 0) {
		Room::shift(xWindowOffset, yWindowOffset);
	}
}