#include "../header/entity/Hitbox.h";

Hitbox::Hitbox() : Hitbox(0, 0, 0, 0) {

}

Hitbox::Hitbox(int x, int y, int w, int h) {
	this->xScaled = this->x = x;
	this->yScaled = this->y = y;
	this->wScaled = this->w = w;
	this->hScaled = this->h = h;
}

bool Hitbox::intersect(Hitbox other) {
	resize();
	other.resize();
	return !(other.yScaled > yScaled + hScaled ||
		other.xScaled > xScaled + wScaled ||
		other.yScaled + other.h < yScaled ||
		other.xScaled + other.w < xScaled);
}

bool Hitbox::within(Hitbox container) {
	resize();
	container.resize();
	return xScaled > container.xScaled &&
		yScaled > container.yScaled &&
		xScaled + wScaled < container.xScaled + container.wScaled &&
		yScaled + hScaled < container.yScaled + container.hScaled;
}

void Hitbox::resize() {
	xScaled = x;
	yScaled = y;
	wScaled = w;
	hScaled = h;
	WindowConfig::convertToPixels(&xScaled, &yScaled);
	WindowConfig::convertToPixels(&wScaled, &hScaled);
}