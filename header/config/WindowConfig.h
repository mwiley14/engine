/*
File: WindowConfig.h

About: Header file for the WindowConfig class. This class is static and not instantiated.
	   The variables and methods contained are used to manage the window dimentions (resizing sprites, finding window hitbox, ect.)

	   More detailed information in WindowConfig.cpp
*/

#ifndef WindowConfig_H
#define WindowConfig_H

#include <SDL.h>

class WindowConfig {

public:

	static int prevW;
	static int prevH;
	static int w;
	static int h;
	static void init(SDL_Window*);
	static void update(int, int);
	static void convertToPixels(int*, int*);
};

#endif