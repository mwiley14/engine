/*
File: EntityList.h

About: This document contains a list of IDs to use to check what type of object an entity object is.
	   Every entity will have the ID set when it is instantiated and this will be refered to in order to confirm which type it is.
*/

#ifndef EntityList_H
#define EntityList_H

#define PLAYER_ID 100

#endif