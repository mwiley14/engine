/*
File: Room.h

About: Header file for the Room class. Contains information about the boundries and background of the game space.

	   More detailed information in Room.cpp
*/

#ifndef Room_H
#define Room_H

#include <vector>
#include "../../entity/Hitbox.h"
#include "../../entity/abstract/Entity.h"

using namespace std;

extern vector<shared_ptr<Entity>> entities;

class Room : public Entity {

public:
	Hitbox innerBounds;
	Room(SDL_Renderer*, string, int, int, int, int);
	static void shift(int, int);
};

extern shared_ptr<Room> room;

#endif