/*
File: TestRoom.h

About: Header file for the TestRoom class. This class is a demonstration of how a static class is used to setup a game space and use
	   a Room object. TODO: Make a room manager or standalone method for room deconstruction.

	   More detailed information in TestRoom.cpp
*/

#ifndef TestRoom_H
#define TestRoom_H

#include <vector>
#include "../entity/Player.h"
#include "../../header/room/abstract/Room.h"
#include "../entity/abstract/Entity.h"

extern vector<shared_ptr<Entity>> entities;

class TestRoom {
public:
	static void setup(SDL_Renderer*);
};

#endif