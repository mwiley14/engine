#ifndef ArrayList_H
#define ArrayList_H

using namespace std;

template <class T>
class ArrayList {

private:
	int storeSize;
	T* store;

public:
	ArrayList() {
		storeSize = 0;
		store = new T[0];
	}
	void push(T t) {
		T* temp = new T[storeSize + 1];
		for (int i = 0; i < storeSize; i++) {
			temp[i] = store[i];
		}
		temp[storeSize] = t;
		storeSize++;
		store = temp;
		delete[] temp;
	}
	int size() {
		return storeSize;
	}
};

#endif