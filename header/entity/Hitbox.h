/*
File: Hitbox.h

About: Header file for the Hitbox class. This class contains information about a given hitbox and methods for calculating collisions.

	   More detailed information in Hitbox.cpp
*/

#ifndef Hitbox_H
#define Hitbox_H

#include "abstract/Resizable.h"

using namespace std;

class Hitbox : public Resizable {

public:
	int x;
	int y;
	int w;
	int h;
	int xScaled;
	int yScaled;
	int wScaled;
	int hScaled;
	Hitbox();
	Hitbox(int, int, int, int);
	bool intersect(Hitbox);
	bool within(Hitbox);
	void resize();
};

#endif