/*
File: Player.h

About: Header file for the Player class. This class acts as a demo playable entity that tests various hitboxes, screen scrolling,
	   and user controls.

	   More detailed information in Player.cpp
*/

#ifndef Player_H
#define Player_H

#include <vector>
#include <memory>
#include "abstract/Entity.h"
#include "abstract/Sprite.h"
#include "../room/abstract/Room.h"
#include "../config/WindowConfig.h"
#include "abstract/Resizable.h"
#include "../config/EntityList.h"

extern shared_ptr<Room> room;

class Player : public Entity {

private:
	int xSpeed = 5;
	int ySpeed = 7;

public:
	Player(SDL_Renderer*, string, int, int, int, int, int);
	static void spawn(SDL_Renderer*, string, int, int, int, int, int);
	void runKeyboardEvents(const Uint8*);
};

#endif