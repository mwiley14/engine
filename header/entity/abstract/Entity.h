/*
File: Entity.h

About: Header file for the Entity abstract class.
	   All entities for the purpose of the game with inherit from this class and universal aspects are managed by this class,
	   such as sprites (or lack thereof), hitbox, whether it is registrered for removal, ect.

	   More detailed information in entity.cpp
*/

#ifndef Entity_H
#define Entity_H

#include <string>
#include <vector>
#include <functional>
#include <boost/optional/optional.hpp>
#include <SDL.h>
#include <SDL_image.h>
#include <memory>
#include "Sprite.h"
#include "../Hitbox.h"

using namespace std;

class Entity {

protected:
	bool removed = false;

public:
	int entityId = 0;
	boost::optional<Sprite> sprite;
	Hitbox hitbox;
	Entity(int, int, int, int);
	void setSprite(Sprite);
	bool isRemoved();
	virtual bool isDrawable();
	virtual bool hasEvents();
	virtual void runEvents();
	virtual void runSDLEvents(SDL_Event);
	virtual void runKeyboardEvents(const Uint8*);
	void registerEntity();
	void remove();
};

extern bool newEntity;
extern vector<shared_ptr<Entity>> entities;
extern vector<shared_ptr<Entity>> removable;

#endif