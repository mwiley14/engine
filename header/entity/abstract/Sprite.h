/*
File: Sprite.h

About: Header file for the Sprite class. The sprite class contains information for an entity sprite and methods for drawing the
	   sprite to the display.

	   More detailed information in Sprite.cpp
*/

#ifndef Drawable_H
#define Drawable_H

#include <SDL_image.h>
#include <string>
#include <vector>
#include "Resizable.h"
#include "../../config/WindowConfig.h"
#include "../Hitbox.h"

using namespace std;

class Sprite : public Resizable {

protected:
	SDL_Texture* texture;
	SDL_Rect sprite;
	Hitbox hitbox;

public:
	int x;
	int y;
	int w;
	int h;
	int zIndex;
	Sprite(SDL_Renderer*, string, int, int, int, int);
	Sprite(SDL_Renderer*, string, int, int, int, int, int);
	void setX(int);
	void setY(int);
	void setW(int);
	void setH(int);
	void draw(SDL_Renderer*);
	void resize();
};

#endif