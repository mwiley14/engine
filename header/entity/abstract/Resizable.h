/*
File: Resizable.h

About: Header for Resizable class. This class acts as an interface for any resizable object (which in this case are all
	   sprites). Resizing is applied every frame where a proper pixel ratio is needed, after the logic is done.

	   More detailed information in Resizable.cpp
*/

#ifndef Resizable_H
#define Resizable_H

#include "../../config/WindowConfig.h"

class Resizable {

public:
	virtual void resize();
};

#endif