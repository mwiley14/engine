/*
File: driver.h

About: Header file for the main driver of the engine.
	   Contains methods for instantiating the classes, starting the frame cycle, and managing entities/sprites.

	   More detailed information in driver.cpp
*/

#ifndef Driver_H
#define Driver_H

#include <Windows.h>
#include <iostream>
#include <SDL.h>
#include <SDL_timer.h>
#include <SDL_image.h>
#include <vector>
#include <ctime>
#include <memory>
#include <algorithm>
#include "../room/abstract/Room.h"
#include "../entity/abstract/Entity.h"
#include "../utils/ArrayList.h"
#include "../config/WindowConfig.h"
#include "../entity/Player.h"
#include "../entity/Hitbox.h"
#include "../entity/abstract/Sprite.h"
#include "../room/TestRoom.h"
#undef main

SDL_Window* window;
SDL_Renderer* renderer;

bool newEntity;
vector<shared_ptr<Entity>> entities;
vector<shared_ptr<Entity>> removable;
shared_ptr<Room> room;

int main();
int WinMain(HINSTANCE, HINSTANCE, LPSTR, int);
int init();
void start();
void clearRemovables();
void sortSprites();

#endif