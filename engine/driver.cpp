/*
File: driver.cpp

About: Contains the entry point for the engine and methods for running the frame cycle and managing entities.
*/

#include "../header/engine/driver.h"

/*
@method
@description - Entry point to the engine.
@return int - Returns 0 on successful run, returns non-0 when an error occurs.
*/
//int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
int main()
{
    if (!init()) {
        printf("Initialization Failed...\n");
        printf("Closing...");
        Sleep(5000);
        return 1;
    }
    start();
    return 0;
}

/*
@method
@description - Initializes the SDL library before use.
@return - Returns 0 on success, non-0 on failure.
*/
int init() {
    //Sets SDL behaviour when the application closes
    atexit(SDL_Quit);
    //Initializing SDL library
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        printf("error initializing SDL: %s\n", SDL_GetError());
        return 1;
    }
    printf("SDL library initialized...\n");
    //Creates window for game to play in
    window = SDL_CreateWindow("Test Game",
        200,
        100,
        1600, 900,
        //SDL_WINDOW_MAXIMIZED);
        SDL_WINDOW_RESIZABLE);
    //0);
    SDL_SetWindowMinimumSize(window, 1600, 900);
    printf("SDL window initialized...\n");
    //Creates graphics renderer
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (!renderer) {
        printf("error creating renderer: %s\n", SDL_GetError());
        return 1;
    }
    printf("SDL renderer created...\n");
    //Records default window proportions
    WindowConfig::init(window);
    return 0;
}

/*
@method
@description - Starts the frame cycle (and sets up the first room).
*/
void start() {
    //Sets up first room
    TestRoom::setup(renderer);
    clock_t fpsTimer = clock();
    bool close = false;
    while (!close) {
        //Only runs the next frame if a 60th of a second has past since the last one (locking at 60 fps)
        if ((clock() - fpsTimer >= 1000 / 60)) {
            fpsTimer = clock();
            SDL_Event event;
            //TODO: filter out unneeded events, such as keyboard
            while (SDL_PollEvent(&event)) {
                //Run non-entity based SDL event scenarios
                switch (event.type) {
                case SDL_QUIT:
                    close = true;
                    break;
                case SDL_WINDOWEVENT:
                    if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                        WindowConfig::update(event.window.data1, event.window.data2);
                    }
                }
                //Run SDL events for all entities for current event
                for (int i = 0; i < entities.size(); i++) {
                    entities.at(i)->runSDLEvents(event);
                }
                break;
            }
            //Get keyboard state and run entity keyboard events & non-entity based keyboard events
            const Uint8* keyboard = SDL_GetKeyboardState(NULL);
            for (int i = 0; i < entities.size(); i++) {
                entities.at(i)->runKeyboardEvents(keyboard);
                entities.at(i)->runEvents();
            }
            if (keyboard[SDL_SCANCODE_ESCAPE]) {
                close = true;
            }
            //Delete entities that have been registered for removal
            if (removable.size() > 0) {
                clearRemovables();
            }
            //Place any new entities into order based on z-index placement (so they get drawn in the correct order)
            if (newEntity) {
                sortSprites();
            }
            //Clears screen and draws entities in current state
            SDL_RenderClear(renderer);
            for (int i = 0; i < entities.size(); i++) {
                if (entities.at(i)->isDrawable()) {
                    entities.at(i)->sprite->draw(renderer);
                }
            }
            SDL_RenderPresent(renderer);
            //This is for testing memory usage
            if (entities.empty()) {
                Player::spawn(renderer, "assets/purple_square.jpg", 500, 500, 250, 250, 100);
                printf("here");
            }
            newEntity = false;
        }
    }
}

/*
@method
@description - Sorts sprites based on z-index. Ensures sprites are drawn with the correct layering. Should only be called when a new
               entity has been added to the entity list.
*/
void sortSprites() {
    sort(entities.begin(), entities.end(), [](shared_ptr<Entity> first, shared_ptr<Entity> second) {
        return first->sprite->zIndex < second->sprite->zIndex;
    });
}

/*
@method
@description - Deletes from the entity list any entities that have been registered for removal. This is run after the events have been
               run but before sprites are redrawn.
*/
//TODO: Finish this later
void clearRemovables() {
    vector<shared_ptr<Entity>> removed;
    vector<shared_ptr<Entity>>::iterator i = entities.begin();
    while (i != entities.end()) {
        bool found = false;
        for (int j = 0; j < removable.size(); j++) {
            if (*i == removable.at(j)) {
                printf("\n%i", *i);
                //removed.push_back(*i);
                i = entities.erase(i);
                //removable.erase(removable.begin() + j);
                found = true;
                break;
            }
        }
        if (!found) {
            i++;
        }
    }
    //for (int i = 0; i < removed.size(); i++) {
    //    delete (removed.at(i));
    //}
    entities.shrink_to_fit();
    removable.clear();
    removable.shrink_to_fit();
}