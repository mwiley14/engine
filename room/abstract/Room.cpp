#include "../../header/room/abstract/Room.h"

Room::Room(SDL_Renderer* renderer, string src, int x, int y, int w, int h) :
	Entity(x, y, w, h),
	innerBounds(200, 200, 600, 600) {
	setSprite(Sprite(renderer, src, x, y, w, h));
}

void Room::shift(int xOffset, int yOffset) {
	for (int i = 0; i < entities.size(); i++) {
		if (!entities.at(i)->isRemoved()) {
			if (entities.at(i)->isDrawable()) {
				entities.at(i)->sprite->x += xOffset;
				entities.at(i)->sprite->y += yOffset;
			}
			entities.at(i)->hitbox.x += xOffset;
			entities.at(i)->hitbox.y += yOffset;
		}
	}
}