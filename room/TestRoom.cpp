#include "../../header/room/TestRoom.h"

void TestRoom::setup(SDL_Renderer* renderer) {
	room = shared_ptr<Room>(new Room(renderer, "assets/village.jpg", -500, -500, 2700, 2700));
	Player::spawn(renderer, "assets/link.png", 500, 500, 40, 80, 200);
	room->registerEntity();
}